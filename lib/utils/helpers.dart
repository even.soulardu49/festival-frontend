import 'package:flutter/material.dart';

/// This file contains some functions which helps to develop

///Calcul height
double calculHeight(double height, BuildContext context) {
  const double ratioHeight = 812;
  return MediaQuery.of(context).size.height / (ratioHeight / height);
}

///Calcul width
double calculWidth(double width, BuildContext context) {
  const double ratioWidth = 375;
  return MediaQuery.of(context).size.width / (ratioWidth / width);
}

///Capitalize the first letter
String capitalize(String? s) {
  if (s != null) {
    return s[0].toUpperCase() + s.substring(1).toLowerCase();
  } else {
    return '';
  }
}

String getDateFromTimestampFormat(String timestampDate) {
  List<String> results1 = timestampDate.split(' ');
  List<String> results2 = results1[0].split('-');
  return results2[2] + '/' + results2[1] + '/' + results2[0];
}

String getHourFromTimestampFormat(String timestampDate) {
  List<String> results1 = timestampDate.split(' ');
  List<String> results2 = results1[1].split(':');
  return results2[0] + 'h' + results2[1];
}

String getDateTimeFromPickerFormat(String date) {
  List<String> results1 = date.split(' ');
  List<String> results2 = results1[1].split(':');
  return results1[0] + " " + results2[0] + ':' + results2[1];
}
