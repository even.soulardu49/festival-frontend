import 'package:festoche/views/home.view.dart';
import 'package:flutter/material.dart';

///This file contains all routes

Map<String, Widget Function(BuildContext)> routes = {
  HomeView.id: (context) => const HomeView(),
};
