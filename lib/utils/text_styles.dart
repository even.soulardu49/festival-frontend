import 'package:festoche/utils/font_weight.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'colors.dart';

TextStyle kButtonStyle(BuildContext context) {
  return GoogleFonts.inter(
    fontSize: 16,
    fontWeight: semiBold,
    color: kWhiteText,
  );
}

TextStyle kBigTitle1(BuildContext context) {
  return GoogleFonts.inter(
    fontSize: 42,
    fontWeight: bold,
    color: kWhiteText,
  );
}

TextStyle kBigTitle2(BuildContext context) {
  return GoogleFonts.inter(
    fontSize: 18,
    fontWeight: bold,
    color: kWhiteText,
  );
}

TextStyle kTitle1(BuildContext context) {
  return GoogleFonts.inter(
    fontSize: 30,
    fontWeight: semiBold,
    color: kWhiteText,
  );
}

TextStyle kTitle2(BuildContext context) {
  return GoogleFonts.inter(
    fontSize: 24,
    fontWeight: semiBold,
    color: kWhiteText,
  );
}

TextStyle kTitle3(BuildContext context) {
  return GoogleFonts.inter(
    fontSize: 24,
    fontWeight: medium,
    color: kWhiteText,
  );
}

TextStyle kTitle4(BuildContext context) {
  return GoogleFonts.inter(
    fontSize: 24,
    fontWeight: regular,
    color: kWhiteText,
  );
}

TextStyle kSpecialTitle1(BuildContext context) {
  return GoogleFonts.inter(
    fontSize: 24,
    fontWeight: medium,
    color: kWhiteText,
  );
}

TextStyle kSpecialTitle2(BuildContext context) {
  return GoogleFonts.inter(
    fontSize: 18,
    fontWeight: medium,
    color: kWhiteText,
  );
}

TextStyle kSubTitle1(BuildContext context) {
  return GoogleFonts.inter(
    fontSize: 16,
    fontWeight: medium,
    color: kWhiteText,
  );
}

TextStyle kSubTitle2(BuildContext context) {
  return GoogleFonts.inter(
    fontSize: 16,
    fontWeight: bold,
    color: kWhiteText,
  );
}

TextStyle kSubTitle3(BuildContext context) {
  return GoogleFonts.inter(
    fontSize: 16,
    fontWeight: semiBold,
    color: kWhiteText,
  );
}

TextStyle kSubTitle4(BuildContext context) {
  return GoogleFonts.inter(
    fontSize: 14,
    fontWeight: medium,
    color: kWhiteText,
  );
}

TextStyle kDescriptionText(BuildContext context) {
  return GoogleFonts.inter(
    fontSize: 16,
    fontWeight: light,
    color: kWhiteText,
  );
}

TextStyle kHintText1(BuildContext context) {
  return GoogleFonts.inter(
    fontSize: 16,
    fontWeight: light,
    color: kDiscretText,
  );
}

TextStyle kClassicText(BuildContext context) {
  return GoogleFonts.inter(
    fontSize: 16,
    fontWeight: medium,
    color: kWhiteText,
  );
}
