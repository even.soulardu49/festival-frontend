import 'dart:io';

import 'package:festoche/utils/response.dart';
import 'package:http/http.dart' as http;

abstract class ConfigAPI {
  static String apiUrl = 'https://api-festival.driing.app/api';

  static Map<String, String> headers = {
    'Content-type': 'application/json',
    'Accept': 'application/json',
    'Authorization': 'Bearer'
  };

  static void setToken(String? token) {
    headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };
  }

  static String getUrlString(String apiCall) {
    return apiUrl + apiCall;
  }

  static Uri getUrl(String apiCall) {
    return Uri.parse(apiUrl + apiCall);
  }

  static Future<Response<ResultType>> errorHandler<ResultType>(
      Future<Response<ResultType>> Function() function) async {
    try {
      return function();
    } on SocketException {
      return Response.error<ResultType>(
        'No Internet connection 😑',
      );
    } on FormatException {
      return Response.error<ResultType>(
        'Bad response format 👎',
      );
    } on HttpException {
      return Response.error<ResultType>(
        "Couldn't find the post 😱",
      );
    } catch (e) {
      return Response.error<ResultType>(e.toString());
    }
  }

  static Future<Response<ResultType>> requestErrorHandler<ResultType>({
    required Future<http.Response> request,
    required Map<int,
            Future<Response<ResultType>> Function(http.Response response)>
        cases,
  }) async {
    try {
      final response = await request;
      print(response.body);
      for (final int code in cases.keys) {
        if (response.statusCode == code) {
          return await cases[code]!(response);
        }
      }
      switch (response.statusCode) {
        case 401:
          {
            return Response.unAuthorized<ResultType>();
          }
      }
      return Response.error<ResultType>('Unhandled: ${response.body}');
    } on SocketException {
      return Response.error<ResultType>(
        'Aucune connexion 😑',
      );
    } on FormatException {
      return Response.error<ResultType>(
        'Mauvais format de réponse 👎',
      );
    } on HttpException {
      return Response.error<ResultType>(
        "Impossible de trouver le résultat 😱",
      );
    } catch (e) {
      print(e);
      return Response.error<ResultType>(e.toString());
    }
  }
}
