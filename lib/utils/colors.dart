import 'dart:ui';

import 'package:flutter/material.dart';

///In this there are all the colors used in the application

const Color kWhiteText = Color(0xFFEFEFEF);
const Color kBlackText = Colors.black;
const Color kDiscretText = Color(0xFFBDBDBD);
const Color kMainBlue = Colors.white10;
const Color kMainBackground = Color(0xFF0E0E0E);
