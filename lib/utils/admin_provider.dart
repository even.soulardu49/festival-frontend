import 'package:festoche/models/admin.dart';
import 'package:festoche/services/api/admin.api.service.dart';
import 'package:festoche/utils/response.dart';
import 'package:flutter/material.dart';

import 'enums.dart';

class AdminProvider with ChangeNotifier {
  Response<Admin>? currentAdminRes;
  bool adminIsLoggedIn = false;

  void _setCurrentAdminRes(Response<Admin> response) {
    currentAdminRes = response;
    print('notify..');
    notifyListeners();
  }

  Future<void> getAdminWithToken() async {
    print('getADminWithToken');
    _setCurrentAdminRes(Response.loading<Admin>());
    final Response<Admin> res = await AdminApiService.getUserWithToken();
    print(res.data);
    if (res.state == ResponseState.complete && res.data != null) {
      adminIsLoggedIn = true;
      _setCurrentAdminRes(res);
    } else {
      adminIsLoggedIn = false;
      notifyListeners();
    }
  }

  void logout() {
    AdminApiService.logout();
    adminIsLoggedIn = false;
    notifyListeners();
  }
}
