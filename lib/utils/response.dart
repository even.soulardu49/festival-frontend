import 'enums.dart';

class Response<ResultType> {
  ResponseState? state;
  ResultType? data;
  String? exception;

  Response({this.state, this.data, this.exception});

  static Response<ResultType> loading<ResultType>() {
    return Response(state: ResponseState.loading);
  }

  static Response<ResultType> complete<ResultType>({ResultType? data}) {
    return Response(state: ResponseState.complete, data: data);
  }

  static Response<ResultType> unAuthorized<ResultType>() {
    return Response(state: ResponseState.unAuthorized);
  }

  static Response<ResultType> error<ResultType>(String exception) {
    return Response(state: ResponseState.error, exception: exception);
  }
}
