import 'package:festoche/models/event.dart';

class Artist {
  Artist({
    this.id,
    required this.name,
    this.description,
    this.imgUrl,
    this.events,
  });

  int? id;
  String name;
  String? description;
  String? imgUrl;
  List<Event>? events;

  factory Artist.fromJson(Map<String, dynamic> json) {
    return Artist(
      id: json['id'],
      name: json['name'],
      description: json['description'],
      imgUrl: json['image'],
      events: json['events'] != null
          ? (json['events'] as List<dynamic>)
              .map((e) => Event.fromJson(e as Map<String, dynamic>))
              .toList()
          : null,
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'description': description,
        'image': imgUrl,
        'events': events,
      };
}
