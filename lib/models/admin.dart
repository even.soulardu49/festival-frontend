class Admin {
  Admin({
    required this.id,
    required this.email,
    this.password,
    this.firstName,
    this.imgUrl,
  });

  int id;
  String? firstName;
  String email;
  String? password;
  String? imgUrl;

  factory Admin.fromJson(Map<String, dynamic> json) {
    return Admin(
      id: json['id'],
      email: json['email'],
      firstName: json['firstName'],
      imgUrl: json['imgUrl'],
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'email': email,
        'password': password,
        'firstName': firstName,
        'imgUrl': imgUrl,
      };
}
