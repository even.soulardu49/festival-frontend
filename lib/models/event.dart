import 'package:festoche/models/artist.dart';

class Event {
  Event({
    this.id,
    required this.name,
    required this.dateTime,
    required this.location,
    this.description,
    this.imgUrl,
    this.artists,
  });

  int? id;
  String name;
  String dateTime;
  String location;
  String? description;
  String? imgUrl;
  List<Artist>? artists;

  factory Event.fromJson(Map<String, dynamic> json) {
    return Event(
      id: json['id'],
      name: json['name'],
      dateTime: json['hours'],
      location: json['location'],
      description: json['description'],
      imgUrl: json['imgUrl'],
      artists: json['artists'] != null
          ? (json['artists'] as List<dynamic>)
              .map((e) => Artist.fromJson(e as Map<String, dynamic>))
              .toList()
          : null,
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'hours': dateTime,
        'location': location,
        'description': description,
        'imgUrl': imgUrl,
        'artists': artists,
      };
}
