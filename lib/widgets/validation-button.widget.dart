import 'package:festoche/utils/colors.dart';
import 'package:flutter/material.dart';

class ValidationButton extends StatelessWidget {
  const ValidationButton({
    Key? key,
    this.onTap,
    this.child,
  }) : super(key: key);

  final void Function()? onTap;
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 50,
        width: double.infinity,
        decoration: const BoxDecoration(
          color: kMainBlue,
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
        child: Center(child: child),
      ),
    );
  }
}
