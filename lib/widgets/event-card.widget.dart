import 'package:festoche/models/event.dart';
import 'package:festoche/utils/colors.dart';
import 'package:festoche/utils/helpers.dart';
import 'package:festoche/utils/text_styles.dart';
import 'package:festoche/views/event/event.view.dart';
import 'package:flutter/material.dart';

class EventCard extends StatelessWidget {
  const EventCard({
    required this.event,
    Key? key,
  }) : super(key: key);

  final Event event;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        if (event.id != null) {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => EventView(
                id: event.id!,
              ),
            ),
          );
        }
      },
      child: Container(
        padding: const EdgeInsets.only(bottom: 15.0, left: 10.0),
        margin: const EdgeInsets.symmetric(horizontal: 5.0),
        height: 200,
        width: 230,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(event.imgUrl ??
                'https://static.thenounproject.com/png/3674270-200.png'),
            fit: BoxFit.cover,
          ),
          color: kDiscretText,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              getDateFromTimestampFormat(event.dateTime),
              style: kSubTitle2(context),
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 2,
            ),
            Text(
              getHourFromTimestampFormat(event.dateTime),
              style: kSubTitle3(context),
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 2,
            ),
            Text(
              capitalize(event.name),
              style: kSubTitle4(context),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
