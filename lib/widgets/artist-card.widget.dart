import 'package:festoche/models/artist.dart';
import 'package:festoche/utils/colors.dart';
import 'package:festoche/utils/text_styles.dart';
import 'package:festoche/views/artist/artist.view.dart';
import 'package:flutter/material.dart';

class ArtistCard extends StatelessWidget {
  const ArtistCard({
    required this.artist,
    Key? key,
  }) : super(key: key);

  final Artist artist;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ArtistView(
                      id: artist.id!,
                    )));
      },
      child: Container(
        padding: const EdgeInsets.only(bottom: 10.0),
        margin: const EdgeInsets.symmetric(horizontal: 5.0),
        height: 160,
        width: 120,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(artist.imgUrl ??
                'https://static.thenounproject.com/png/3674270-200.png'),
            fit: BoxFit.cover,
          ),
          color: kDiscretText,
        ),
        child: Align(
          alignment: Alignment.bottomCenter,
          child: Text(
            artist.name.toUpperCase(),
            style: kBigTitle2(context),
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
