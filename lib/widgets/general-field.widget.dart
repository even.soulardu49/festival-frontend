import 'package:festoche/utils/text_styles.dart';
import 'package:flutter/material.dart';

class GeneralField extends StatelessWidget {
  const GeneralField({
    Key? key,
    this.controller,
    this.hintText,
    this.obscureText,
  }) : super(key: key);

  final TextEditingController? controller;
  final String? hintText;
  final bool? obscureText;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      obscureText: obscureText ?? false,
      decoration: InputDecoration(
        isDense: true,
        contentPadding: const EdgeInsets.symmetric(
          horizontal: 20.0,
          vertical: 16.0,
        ),
        focusedBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(24.0),
          ),
          borderSide: BorderSide(color: Colors.white, width: 1.0),
        ),
        enabledBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(24.0)),
          borderSide: BorderSide(color: Colors.white, width: 1.0),
        ),
        hintText: hintText,
        hintStyle: kHintText1(context),
      ),
      style: kClassicText(context),
    );
  }
}
