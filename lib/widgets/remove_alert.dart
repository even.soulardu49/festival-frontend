import 'package:festoche/utils/colors.dart';
import 'package:festoche/utils/text_styles.dart';
import 'package:flutter/material.dart';

Future<void> removeAlert(
    {required BuildContext context, required Function() onTap}) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext buildContext) {
      return AlertDialog(
        contentPadding: const EdgeInsets.only(top: 20),
        backgroundColor: kMainBackground,
        elevation: 10.0,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(36),
          ),
        ),
        content: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(
                  right: 40,
                  left: 40,
                  bottom: 25,
                ),
                child: Column(
                  children: [
                    Text(
                      'Vous allez supprimer cet élément',
                      textAlign: TextAlign.center,
                      style: kTitle4(buildContext),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Toutes les informations seront supprimées et seront irrécupérables.',
                      textAlign: TextAlign.center,
                      style: kClassicText(context),
                    ),
                  ],
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () {
                        Navigator.pop(buildContext);
                      },
                      child: Container(
                        height: 60,
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(36.0),
                          ), // BorderRadius
                        ), // BoxDecoration
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border(
                              top: BorderSide(
                                  width: 0.5, color: Colors.grey[200]!),
                            ), // BorderRadius
                          ),
                          child: Center(
                            child: Text(
                              'Annuler',
                              style: kClassicText(buildContext),
                            ),
                          ), // BoxDecoration
                        ), // Container
                      ),
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: onTap,
                      child: Container(
                        height: 60,
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(36.0),
                          ), // BorderRadius
                        ), // BoxDecoration
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border(
                              left: BorderSide(
                                  width: 0.5, color: Colors.grey[200]!),
                              top: BorderSide(
                                  width: 0.5, color: Colors.grey[200]!),
                            ), // BorderRadius
                          ),
                          child: Center(
                            child: Text(
                              'Continuer',
                              style: kClassicText(buildContext),
                              textAlign: TextAlign.center,
                            ),
                          ), // BoxDecoration
                        ), // Container
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    },
  );
}
