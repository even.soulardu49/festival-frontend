// ignore_for_file: invariant_booleans, null_check_on_nullable_type_parameter

import 'package:festoche/services/toastr.service.dart';
import 'package:festoche/utils/enums.dart';
import 'package:festoche/utils/response.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';

class DResponseBuilder<ResultType> extends StatelessWidget {
  final WidgetBuilder loadingBuilder;
  final Widget Function(BuildContext context, ResultType? value)
      completeBuilder;
  final WidgetBuilder? builder;
  final bool alertUnAuthorized;
  final Response<ResultType> response;

  const DResponseBuilder(
      {required this.loadingBuilder,
      required this.completeBuilder,
      this.builder,
      required this.response,
      this.alertUnAuthorized = false});

  @override
  Widget build(BuildContext context) {
    switch (response.state) {
      //If response is loading, display a custom loader (shimmer)
      case ResponseState.loading:
        return loadingBuilder(context);
      //If response is complete, so we have data, call the completeBuilder (can be redirect, showMessage, or add a widget)
      case ResponseState.complete:
        return completeBuilder(context, response.data);
      //If response is an error, show a message and display a default builder (default button text instead of nothing)
      case ResponseState.error:
        WidgetsBinding.instance!.addPostFrameCallback((_) {
          ToastrService.show(context, response.exception!,
              type: ToastrType.error);
        });
        if (builder != null) return builder!(context);
        break;
      //If response is unauthorized, logout the user
      case ResponseState.unAuthorized:
        if (alertUnAuthorized) {
          WidgetsBinding.instance!.addPostFrameCallback((_) {
            ToastrService.show(
                context, 'Les informations de login sont incorrectes',
                type: ToastrType.error);
          });
          if (builder != null) return builder!(context);
        } else {
          //Navigator.of(context)
          //  .pushNamedAndRemoveUntil(SplashScreen.route, (route) => false);
          //context.read<SystemProvider>().setAuthentificated(false);
        }
        break;
      //If response's state is null, return default builder
      case null:
        if (builder != null) return builder!(context);
        break;
    }
    if (builder != null) return builder!(context);
    return Container();
  }
}
