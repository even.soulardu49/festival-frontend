import 'package:festoche/models/artist.dart';
import 'package:festoche/models/event.dart';
import 'package:festoche/utils/admin_provider.dart';
import 'package:festoche/utils/colors.dart';
import 'package:festoche/utils/helpers.dart';
import 'package:festoche/utils/text_styles.dart';
import 'package:festoche/views/event/set-event/set-event.view.dart';
import 'package:festoche/views/login/login.view.dart';
import 'package:festoche/widgets/artist-card.widget.dart';
import 'package:festoche/widgets/d_response_builder.dart';
import 'package:festoche/widgets/event-card.widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'artist/set-artist/set-artist.view.dart';
import 'home.view_models.dart';

class HomeView extends StatelessWidget {
  static const String id = 'home-view';

  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProxyProvider<AdminProvider, HomeViewModels>(
      update: (context, sys, previousHome) => previousHome!..updates(sys),
      create: (BuildContext context) => HomeViewModels(),
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            decoration: const BoxDecoration(
              color: kMainBackground,
              image: DecorationImage(
                alignment: Alignment.topCenter,
                image: AssetImage('assets/logo/background.jpg'),
              ),
            ),
            child: Consumer<AdminProvider>(
              builder: (context, adminVm, child) => Consumer<HomeViewModels>(
                builder: (context, homeVm, child) => Column(
                  children: [
                    SizedBox(
                      height: 230,
                      width: double.infinity,
                      child: Stack(
                        alignment: Alignment.topCenter,
                        children: [
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => const LoginView()));
                            },
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(top: 40.0, right: 20.0),
                              child: Align(
                                alignment: Alignment.topRight,
                                child: adminVm.adminIsLoggedIn
                                    ? GestureDetector(
                                        behavior: HitTestBehavior.translucent,
                                        onTap: () {
                                          context
                                              .read<AdminProvider>()
                                              .logout();
                                        },
                                        child: const Icon(
                                          Icons.logout,
                                          color: Colors.white,
                                        ),
                                      )
                                    : Image.asset(
                                        'assets/icon/manage_accounts.png',
                                        width: calculWidth(27, context),
                                        color: Colors.white,
                                      ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(
                          top: 40.0, right: 20.0, left: 20.0, bottom: 30.0),
                      decoration: const BoxDecoration(
                        color: kMainBackground,
                        borderRadius: BorderRadius.vertical(
                          top: Radius.circular(30),
                        ),
                      ),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                'Découvrez les artistes',
                                style: kSpecialTitle2(context),
                              ),
                              adminVm.adminIsLoggedIn
                                  ? GestureDetector(
                                      behavior: HitTestBehavior.translucent,
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    const SetArtistView()));
                                      },
                                      child: const Icon(
                                        Icons.add,
                                        color: kWhiteText,
                                        size: 18,
                                      ),
                                    )
                                  : Container(),
                            ],
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 15.0),
                            height: 160,
                            child: DResponseBuilder(
                              response: homeVm.artistsResponse,
                              loadingBuilder: (BuildContext context) {
                                return Container();
                              },
                              completeBuilder: (BuildContext context,
                                  List<Artist>? artists) {
                                if (artists != null) {
                                  return ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: artists.length,
                                    itemBuilder: (context, index) {
                                      return ArtistCard(
                                        artist: artists[index],
                                      );
                                    },
                                  );
                                } else {
                                  return Container();
                                }
                              },
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Découvrez les évènements',
                                style: kSpecialTitle2(context),
                              ),
                              adminVm.adminIsLoggedIn
                                  ? GestureDetector(
                                      behavior: HitTestBehavior.translucent,
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    const SetEventView()));
                                      },
                                      child: const Icon(
                                        Icons.add,
                                        color: kWhiteText,
                                        size: 18,
                                      ),
                                    )
                                  : Container(),
                            ],
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 15.0),
                            height: 200,
                            child: DResponseBuilder(
                              response: homeVm.eventsResponse,
                              loadingBuilder: (BuildContext context) {
                                return Container();
                              },
                              completeBuilder:
                                  (BuildContext context, List<Event>? events) {
                                if (events != null) {
                                  return ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: events.length,
                                    itemBuilder: (context, index) {
                                      return EventCard(
                                        event: events[index],
                                      );
                                    },
                                  );
                                } else {
                                  return Container();
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
