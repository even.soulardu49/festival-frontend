import 'package:festoche/models/artist.dart';
import 'package:festoche/models/event.dart';
import 'package:festoche/services/api/artist.api.service.dart';
import 'package:festoche/services/api/event.api.service.dart';
import 'package:festoche/utils/admin_provider.dart';
import 'package:festoche/utils/response.dart';
import 'package:flutter/material.dart';

class HomeViewModels extends ChangeNotifier {
  late AdminProvider systemProvider;
  Response<List<Artist>> artistsResponse = Response<List<Artist>>();
  Response<List<Event>> eventsResponse = Response<List<Event>>();

  void updates(AdminProvider provider) => systemProvider = provider;

  HomeViewModels() {
    Future.wait([
      ArtistApiService.getAllArtist(),
      EventApiService.getAllEvents(),
    ]).then((List<Response<List<Object>>> results) {
      artistsResponse = results[0] as Response<List<Artist>>;
      eventsResponse = results[1] as Response<List<Event>>;
      notifyListeners();
    });
  }
}
