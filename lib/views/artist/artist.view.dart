import 'package:festoche/models/artist.dart';
import 'package:festoche/utils/admin_provider.dart';
import 'package:festoche/utils/colors.dart';
import 'package:festoche/utils/text_styles.dart';
import 'package:festoche/views/artist/artist.view_models.dart';
import 'package:festoche/views/artist/set-artist/set-artist.view.dart';
import 'package:festoche/widgets/d_response_builder.dart';
import 'package:festoche/widgets/event-card.widget.dart';
import 'package:festoche/widgets/remove_alert.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../home.view.dart';

class ArtistView extends StatelessWidget {
  const ArtistView({Key? key, required this.id}) : super(key: key);

  final int id;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ArtistViewModels>(
      create: (BuildContext context) => ArtistViewModels(id),
      child: Scaffold(
        backgroundColor: kMainBackground,
        body: SingleChildScrollView(
          child: Consumer<AdminProvider>(
            builder: (context, adminProvider, child) =>
                Consumer<ArtistViewModels>(
              builder: (context, artistVm, child) => DResponseBuilder(
                response: artistVm.artistResponse,
                loadingBuilder: (BuildContext context) {
                  return Container();
                },
                completeBuilder: (BuildContext context, Artist? artist) {
                  if (artist != null) {
                    return Column(
                      children: [
                        Stack(
                          alignment: Alignment.bottomRight,
                          children: [
                            Image.network(
                              artist.imgUrl ??
                                  'https://static.thenounproject.com/png/3674270-200.png',
                              height: 300,
                              width: double.infinity,
                              fit: BoxFit.cover,
                              alignment: Alignment.topCenter,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(right: 10.0, top: 5.0),
                              child: Text(
                                artist.name.toUpperCase(),
                                style: kBigTitle1(context),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          padding: const EdgeInsets.only(
                              top: 40.0, right: 20.0, left: 20.0, bottom: 30.0),
                          decoration: const BoxDecoration(
                            color: kMainBackground,
                            borderRadius: BorderRadius.vertical(
                              top: Radius.circular(30),
                            ),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              adminProvider.adminIsLoggedIn
                                  ? Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        GestureDetector(
                                          behavior: HitTestBehavior.translucent,
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        SetArtistView(
                                                          artist: artist,
                                                        )));
                                          },
                                          child: const Icon(
                                            Icons.create,
                                            color: kWhiteText,
                                          ),
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        GestureDetector(
                                          behavior: HitTestBehavior.translucent,
                                          onTap: () async {
                                            await removeAlert(
                                                context: context,
                                                onTap: () async {
                                                  bool isRemove = await context
                                                      .read<ArtistViewModels>()
                                                      .remove();
                                                  if (isRemove) {
                                                    Navigator.pushAndRemoveUntil(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                const HomeView()),
                                                        (route) => false);
                                                  }
                                                });
                                          },
                                          child: const Align(
                                            alignment: Alignment.topRight,
                                            child: Icon(
                                              Icons.delete,
                                              color: kWhiteText,
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  : Container(),
                              Text(
                                'A propos',
                                style: kTitle3(context),
                              ),
                              Container(
                                margin:
                                    const EdgeInsets.only(top: 15.0, left: 5.0),
                                child: Text(
                                  '${artist.description}',
                                  style: kDescriptionText(context),
                                ),
                              ),
                              const SizedBox(
                                height: 40,
                              ),
                              Text(
                                'Apparitions',
                                style: kTitle3(context),
                              ),
                              Container(
                                margin: const EdgeInsets.only(top: 15.0),
                                height: 200,
                                child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  itemCount: artist.events?.length ?? 0,
                                  itemBuilder: (context, index) {
                                    if (artist.events != null) {
                                      return EventCard(
                                        event: artist.events![index],
                                      );
                                    } else {
                                      return Container();
                                    }
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  } else {
                    return Container();
                  }
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}
