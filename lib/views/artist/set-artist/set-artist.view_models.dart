import 'package:festoche/models/artist.dart';
import 'package:festoche/models/event.dart';
import 'package:festoche/services/api/artist.api.service.dart';
import 'package:festoche/services/api/event.api.service.dart';
import 'package:festoche/utils/response.dart';
import 'package:flutter/material.dart';

class SetArtistViewModels extends ChangeNotifier {
  TextEditingController tempNameController = TextEditingController();
  TextEditingController tempDescriptionController = TextEditingController();
  TextEditingController tempImageUrlController = TextEditingController();
  Response<List<Event>> eventsResponse = Response<List<Event>>();
  List<Event> artistEvents = [];
  int? artistId;

  SetArtistViewModels(Artist? artist) {
    if (artist != null) {
      artistId = artist.id;
      tempNameController.text = artist.name;
      tempDescriptionController.text = artist.description ?? '';
      tempImageUrlController.text = artist.imgUrl ?? '';
      artistEvents = artist.events ?? [];
    }
    EventApiService.getAllEvents().then((Response<List<Event>> result) {
      eventsResponse = result;
      notifyListeners();
    });
  }

  Future<bool> save() async {
    bool isSave = false;
    if (tempNameController.text.isNotEmpty &&
        tempDescriptionController.text.isNotEmpty &&
        tempImageUrlController.text.isNotEmpty) {
      if (artistId != null) {
        try {
          await ArtistApiService.updateArtist(Artist(
            id: artistId,
            name: tempNameController.text,
            description: tempDescriptionController.text,
            imgUrl: tempImageUrlController.text,
            events: artistEvents,
          ));
          isSave = true;
        } catch (e) {
          print(e);
        }
      } else {
        try {
          await ArtistApiService.addArtist(Artist(
            name: tempNameController.text,
            description: tempDescriptionController.text,
            imgUrl: tempImageUrlController.text,
            events: artistEvents,
          ));
          isSave = true;
        } catch (e) {
          print(e);
        }
      }
    }

    return isSave;
  }

  void setArtistEvents(List<Event> events) {
    artistEvents = events;
    notifyListeners();
  }
}
