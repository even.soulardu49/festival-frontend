import 'package:festoche/models/artist.dart';
import 'package:festoche/models/event.dart';
import 'package:festoche/services/toastr.service.dart';
import 'package:festoche/utils/colors.dart';
import 'package:festoche/utils/enums.dart';
import 'package:festoche/utils/text_styles.dart';
import 'package:festoche/views/artist/set-artist/set-artist.view_models.dart';
import 'package:festoche/views/home.view.dart';
import 'package:festoche/widgets/general-field.widget.dart';
import 'package:festoche/widgets/validation-button.widget.dart';
import 'package:flutter/material.dart';
import 'package:multiselect/multiselect.dart';
import 'package:provider/provider.dart';

class SetArtistView extends StatelessWidget {
  const SetArtistView({Key? key, this.artist}) : super(key: key);

  final Artist? artist;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<SetArtistViewModels>(
      create: (BuildContext context) => SetArtistViewModels(artist),
      builder: (context, _) {
        return Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: kMainBackground,
          body: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 30.0, vertical: 40.0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: const Align(
                      alignment: Alignment.topLeft,
                      child: Icon(
                        Icons.arrow_back_ios,
                        size: 30,
                        color: kWhiteText,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Text(
                    'Ajouter/Modifier l\'artiste',
                    style: kTitle1(context),
                  ),
                  const SizedBox(
                    height: 70,
                  ),
                  Text(
                    'Informations',
                    style: kClassicText(context),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  GeneralField(
                    controller:
                        context.read<SetArtistViewModels>().tempNameController,
                    hintText: 'Nom',
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  GeneralField(
                    controller: context
                        .read<SetArtistViewModels>()
                        .tempImageUrlController,
                    hintText: 'Image url',
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  GeneralField(
                    controller: context
                        .read<SetArtistViewModels>()
                        .tempDescriptionController,
                    hintText: 'Description',
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Text(
                    'Évènements',
                    style: kClassicText(context),
                  ),
                  Consumer<SetArtistViewModels>(
                    builder: (context, vm, child) => DropDownMultiSelect(
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: const EdgeInsets.symmetric(
                          horizontal: 20.0,
                          vertical: 16.0,
                        ),
                        focusedBorder: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(24.0),
                          ),
                          borderSide:
                              BorderSide(color: Colors.white, width: 1.0),
                        ),
                        enabledBorder: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(24.0)),
                          borderSide:
                              BorderSide(color: Colors.white, width: 1.0),
                        ),
                        hintStyle: kHintText1(context),
                        labelStyle: kHintText1(context),
                      ),
                      onChanged: (List<String> selectedList) {
                        List<Event> events = vm.eventsResponse.data
                                ?.where((e) => selectedList.contains(e.name))
                                .toList() ??
                            [];
                        context
                            .read<SetArtistViewModels>()
                            .setArtistEvents(events);
                      },
                      options:
                          vm.eventsResponse.data?.map((e) => e.name).toList() ??
                              [""],
                      selectedValues:
                          vm.artistEvents.map((e) => e.name).toList(),
                      whenEmpty: '',
                    ),
                  ),
                  ValidationButton(
                    child: Text(
                      'VALIDER',
                      style: kButtonStyle(context),
                    ),
                    onTap: () async {
                      bool added =
                          await context.read<SetArtistViewModels>().save();
                      if (added) {
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const HomeView()),
                            (route) => false);
                      } else {
                        ToastrService.show(
                            context, 'Veuillez rentrer toutes les informations',
                            type: ToastrType.info);
                      }
                    },
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
