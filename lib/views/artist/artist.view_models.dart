import 'package:festoche/models/artist.dart';
import 'package:festoche/services/api/artist.api.service.dart';
import 'package:festoche/utils/response.dart';
import 'package:flutter/material.dart';

class ArtistViewModels extends ChangeNotifier {
  Response<Artist> artistResponse = Response<Artist>();

  ArtistViewModels(int selectedId) {
    ArtistApiService.getArtistById(selectedId).then((Response<Artist> result) {
      artistResponse = result;
      notifyListeners();
    });
  }

  Future<bool> remove() async {
    bool isSave = false;
    if (artistResponse.data?.id != null) {
      try {
        await ArtistApiService.removeArtist(artistResponse.data!.id!);
        isSave = true;
      } catch (e) {
        print(e);
      }
    }

    return isSave;
  }
}
