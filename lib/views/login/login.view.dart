import 'package:festoche/services/toastr.service.dart';
import 'package:festoche/utils/admin_provider.dart';
import 'package:festoche/utils/colors.dart';
import 'package:festoche/utils/enums.dart';
import 'package:festoche/utils/response.dart';
import 'package:festoche/utils/text_styles.dart';
import 'package:festoche/views/login/login.view_models.dart';
import 'package:festoche/widgets/d_response_builder.dart';
import 'package:festoche/widgets/general-field.widget.dart';
import 'package:festoche/widgets/validation-button.widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginView extends StatelessWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProxyProvider<AdminProvider, LoginViewModels>(
      update: (context, sys, previousLogin) => previousLogin!..updates(sys),
      create: (BuildContext context) => LoginViewModels(),
      builder: (BuildContext context, _) {
        return Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: kMainBackground,
          body: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 30.0, vertical: 40.0),
            child: Column(
              children: [
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: const Align(
                    alignment: Alignment.topLeft,
                    child: Icon(
                      Icons.arrow_back_ios,
                      size: 30,
                      color: kWhiteText,
                    ),
                  ),
                ),
                const Spacer(),
                Text(
                  'Connectez-vous',
                  style: kTitle1(context),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  constraints: const BoxConstraints(
                    maxWidth: 230.0,
                  ),
                  child: Text(
                    'Pour accéder au fonctionnalités administrateur',
                    style: kSubTitle1(context),
                    textAlign: TextAlign.center,
                  ),
                ),
                const SizedBox(
                  height: 80,
                ),
                GeneralField(
                  hintText: 'Email',
                  controller:
                      context.read<LoginViewModels>().tempEmailController,
                ),
                const SizedBox(
                  height: 30,
                ),
                GeneralField(
                  hintText: 'Mot de passe',
                  obscureText: true,
                  controller:
                      context.read<LoginViewModels>().tempPasswordController,
                ),
                const SizedBox(
                  height: 80,
                ),
                ValidationButton(
                  onTap: () async {
                    await context.read<LoginViewModels>().loginWithEmail();
                    Navigator.pop(context);
                  },
                  child: Center(
                    child: Selector<LoginViewModels, Response<void>>(
                      selector: (context, provider) => provider.loginResponse,
                      builder: (context, requestRes, child) {
                        return DResponseBuilder<void>(
                          response: requestRes,
                          alertUnAuthorized: true,
                          loadingBuilder: (context) {
                            return const CircularProgressIndicator();
                          },
                          completeBuilder: (context, value) {
                            WidgetsBinding.instance!.addPostFrameCallback((_) {
                              //Navigator.of(context).pushNamed(Root.route);
                              ToastrService.show(context, "Amuse-toi bien :D",
                                  type: ToastrType.success);
                            });
                            return Container();
                          },
                          builder: (context) {
                            return Text(
                              'SE CONNECTER',
                              style: kButtonStyle(context),
                            );
                          },
                        );
                      },
                    ),
                  ),
                ),
                const Spacer(),
              ],
            ),
          ),
        );
      },
    );
  }
}
