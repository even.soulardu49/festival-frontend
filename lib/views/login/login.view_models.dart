import 'package:festoche/services/api/admin.api.service.dart';
import 'package:festoche/utils/admin_provider.dart';
import 'package:festoche/utils/response.dart';
import 'package:flutter/material.dart';

class LoginViewModels extends ChangeNotifier {
  AdminProvider? _systemProvider;
  TextEditingController tempEmailController = TextEditingController();
  TextEditingController tempPasswordController = TextEditingController();
  Response<void> loginResponse = Response<void>();

  void updates(AdminProvider provider) => _systemProvider = provider;

  Future<void> loginWithEmail() async {
    try {
      loginResponse = await AdminApiService.loginWithEmail(
        tempEmailController.text,
        tempPasswordController.text,
      );
      await _systemProvider!.getAdminWithToken();
      _setLoginResponse(loginResponse);
      tempEmailController.clear();
      tempPasswordController.clear();
    } catch (exception) {
      _setLoginResponse(Response.error<void>(exception.toString()));
    }
  }

  void _setLoginResponse(Response<void> response) {
    loginResponse = response;
    notifyListeners();
  }
}
