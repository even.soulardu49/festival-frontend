import 'package:festoche/models/artist.dart';
import 'package:festoche/models/event.dart';
import 'package:festoche/services/toastr.service.dart';
import 'package:festoche/utils/colors.dart';
import 'package:festoche/utils/enums.dart';
import 'package:festoche/utils/helpers.dart';
import 'package:festoche/utils/text_styles.dart';
import 'package:festoche/views/event/set-event/set-event.view_models.dart';
import 'package:festoche/widgets/general-field.widget.dart';
import 'package:festoche/widgets/outlined_button.dart';
import 'package:festoche/widgets/validation-button.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:multiselect/multiselect.dart';
import 'package:provider/provider.dart';

import '../../home.view.dart';

class SetEventView extends StatelessWidget {
  const SetEventView({Key? key, this.event}) : super(key: key);

  final Event? event;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<SetEventViewModels>(
        create: (BuildContext context) => SetEventViewModels(event),
        builder: (context, _) {
          return Scaffold(
            resizeToAvoidBottomInset: false,
            backgroundColor: kMainBackground,
            body: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 30.0, vertical: 40.0),
              child: SingleChildScrollView(
                child: Consumer<SetEventViewModels>(
                  builder: (context, vm, child) => Column(
                    children: [
                      GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: const Align(
                          alignment: Alignment.topLeft,
                          child: Icon(
                            Icons.arrow_back_ios,
                            size: 30,
                            color: kWhiteText,
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      Text(
                        'Ajouter/Modifier l\'évènement',
                        style: kTitle1(context),
                      ),
                      const SizedBox(
                        height: 70,
                      ),
                      Text(
                        'Informations',
                        style: kClassicText(context),
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      GeneralField(
                        controller: context
                            .read<SetEventViewModels>()
                            .tempNameController,
                        hintText: 'Nom',
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      GeneralField(
                        controller: context
                            .read<SetEventViewModels>()
                            .tempLocationController,
                        hintText: 'Adresse',
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      GeneralField(
                        controller: context
                            .read<SetEventViewModels>()
                            .tempImageUrlController,
                        hintText: 'Image url',
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      GeneralField(
                        controller: context
                            .read<SetEventViewModels>()
                            .tempDescriptionController,
                        hintText: 'Description',
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      FOutlinedButton(
                        height: 0,
                        icon: const Icon(Icons.arrow_drop_down,
                            color: kDiscretText),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 14),
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        strokeWidth: 1,
                        radius: 24,
                        gradient: const LinearGradient(
                          colors: [kWhiteText, kWhiteText],
                        ),
                        onPressed: () {
                          DatePicker.showDateTimePicker(
                            context,
                            showTitleActions: true,
                            minTime: DateTime(DateTime.now().year,
                                DateTime.now().month, DateTime.now().day),
                            currentTime: DateTime.now(),
                            onConfirm: (date) {
                              context
                                  .read<SetEventViewModels>()
                                  .setTempDateTime(getDateTimeFromPickerFormat(
                                      date.toString()));
                            },
                            locale: LocaleType.fr,
                            theme: DatePickerTheme(
                              cancelStyle: GoogleFonts.montserrat(
                                color: Colors.white70,
                                fontSize: 16,
                              ),
                              doneStyle: GoogleFonts.montserrat(
                                color: Colors.blue,
                                fontSize: 16,
                              ),
                              itemStyle: GoogleFonts.montserrat(
                                color: Colors.white,
                                fontSize: 18,
                              ),
                              backgroundColor: kMainBackground,
                            ),
                          );
                        },
                        child: vm.tempDateTime.isEmpty
                            ? Text(
                                'Date',
                                style: kHintText1(context),
                              )
                            : Text(
                                vm.tempDateTime,
                                style: kClassicText(context),
                              ),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      Text(
                        'Artistes',
                        style: kClassicText(context),
                      ),
                      DropDownMultiSelect(
                        decoration: InputDecoration(
                          isDense: true,
                          contentPadding: const EdgeInsets.symmetric(
                            horizontal: 20.0,
                            vertical: 16.0,
                          ),
                          focusedBorder: const OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(24.0),
                            ),
                            borderSide:
                                BorderSide(color: Colors.white, width: 1.0),
                          ),
                          enabledBorder: const OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(24.0)),
                            borderSide:
                                BorderSide(color: Colors.white, width: 1.0),
                          ),
                          hintStyle: kHintText1(context),
                          labelStyle: kHintText1(context),
                        ),
                        onChanged: (List<String> selectedList) {
                          List<Artist> artists = vm.artistsResponse.data
                                  ?.where((e) => selectedList.contains(e.name))
                                  .toList() ??
                              [];
                          context
                              .read<SetEventViewModels>()
                              .setEventArtists(artists);
                        },
                        options: vm.artistsResponse.data
                                ?.map((e) => e.name)
                                .toList() ??
                            [""],
                        selectedValues:
                            vm.eventArtists.map((e) => e.name).toList(),
                        whenEmpty: '',
                      ),
                      ValidationButton(
                        child: Text(
                          'VALIDER',
                          style: kButtonStyle(context),
                        ),
                        onTap: () async {
                          bool added =
                              await context.read<SetEventViewModels>().save();
                          if (added) {
                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const HomeView()),
                                (route) => false);
                          } else {
                            ToastrService.show(context,
                                'Veuillez rentrer toutes les informations',
                                type: ToastrType.info);
                          }
                        },
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }
}
