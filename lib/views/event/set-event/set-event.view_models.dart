import 'package:festoche/models/artist.dart';
import 'package:festoche/models/event.dart';
import 'package:festoche/services/api/artist.api.service.dart';
import 'package:festoche/services/api/event.api.service.dart';
import 'package:festoche/utils/response.dart';
import 'package:flutter/material.dart';

class SetEventViewModels extends ChangeNotifier {
  TextEditingController tempNameController = TextEditingController();
  TextEditingController tempDescriptionController = TextEditingController();
  TextEditingController tempLocationController = TextEditingController();
  TextEditingController tempImageUrlController = TextEditingController();
  String tempDateTime = '';
  Response<List<Artist>> artistsResponse = Response<List<Artist>>();
  List<Artist> eventArtists = [];
  int? eventId;

  SetEventViewModels(Event? event) {
    if (event != null) {
      eventId = event.id;
      tempNameController.text = event.name;
      tempLocationController.text = event.location;
      tempDescriptionController.text = event.description ?? '';
      tempDateTime = event.dateTime;
      tempImageUrlController.text = event.imgUrl ?? '';
      eventArtists = event.artists ?? [];
    }
    ArtistApiService.getAllArtist().then((Response<List<Artist>> result) {
      artistsResponse = result;
      notifyListeners();
    });
  }

  Future<bool> save() async {
    bool isSave = false;
    if (tempNameController.text.isNotEmpty &&
        tempDescriptionController.text.isNotEmpty &&
        tempImageUrlController.text.isNotEmpty &&
        tempLocationController.text.isNotEmpty &&
        tempDateTime.isNotEmpty) {
      if (eventId != null) {
        try {
          await EventApiService.updateEvent(Event(
            id: eventId!,
            name: tempNameController.text,
            description: tempDescriptionController.text,
            imgUrl: tempImageUrlController.text,
            artists: eventArtists,
            dateTime: tempDateTime,
            location: tempLocationController.text,
          ));
          isSave = true;
        } catch (e) {
          print(e);
        }
      } else {
        try {
          await EventApiService.addEvent(Event(
            name: tempNameController.text,
            description: tempDescriptionController.text,
            imgUrl: tempImageUrlController.text,
            artists: eventArtists,
            dateTime: tempDateTime,
            location: tempLocationController.text,
          ));
          isSave = true;
        } catch (e) {
          print(e);
        }
      }
    }

    return isSave;
  }

  void setEventArtists(List<Artist> artists) {
    eventArtists = artists;
    notifyListeners();
  }

  void setTempDateTime(String temp) {
    tempDateTime = temp;
    notifyListeners();
  }
}
