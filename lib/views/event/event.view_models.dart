import 'package:festoche/models/event.dart';
import 'package:festoche/services/api/event.api.service.dart';
import 'package:festoche/utils/response.dart';
import 'package:flutter/material.dart';

class EventViewModels extends ChangeNotifier {
  int? selectedId;
  Response<Event> eventsResponse = Response<Event>();

  EventViewModels(int selectedId) {
    EventApiService.getEventById(selectedId).then((Response<Event> result) {
      eventsResponse = result;
      notifyListeners();
    });
  }

  Future<bool> remove() async {
    bool isSave = false;
    if (eventsResponse.data?.id != null) {
      try {
        await EventApiService.removeEvent(eventsResponse.data!.id!);
        isSave = true;
      } catch (e) {
        print(e);
      }
    }

    return isSave;
  }
}
