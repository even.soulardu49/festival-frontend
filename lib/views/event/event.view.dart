import 'package:festoche/models/event.dart';
import 'package:festoche/utils/admin_provider.dart';
import 'package:festoche/utils/colors.dart';
import 'package:festoche/utils/helpers.dart';
import 'package:festoche/utils/text_styles.dart';
import 'package:festoche/views/event/event.view_models.dart';
import 'package:festoche/views/event/set-event/set-event.view.dart';
import 'package:festoche/widgets/artist-card.widget.dart';
import 'package:festoche/widgets/d_response_builder.dart';
import 'package:festoche/widgets/remove_alert.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../home.view.dart';

class EventView extends StatelessWidget {
  const EventView({Key? key, required this.id}) : super(key: key);

  final int id;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<EventViewModels>(
      create: (BuildContext context) => EventViewModels(id),
      child: Scaffold(
        backgroundColor: kMainBackground,
        body: SingleChildScrollView(
          child: Consumer<AdminProvider>(
            builder: (context, adminProvider, child) =>
                Consumer<EventViewModels>(
              builder: (context, eventVm, child) => DResponseBuilder(
                response: eventVm.eventsResponse,
                loadingBuilder: (BuildContext context) {
                  return Container();
                },
                completeBuilder: (BuildContext context, Event? event) {
                  if (event != null) {
                    return Column(
                      children: [
                        Stack(
                          alignment: Alignment.bottomRight,
                          children: [
                            Image.network(
                              event.imgUrl ??
                                  'https://static.thenounproject.com/png/3674270-200.png',
                              height: 300,
                              width: double.infinity,
                              fit: BoxFit.cover,
                              alignment: Alignment.topCenter,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  right: 10.0, bottom: 10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    getDateFromTimestampFormat(event.dateTime),
                                    style: kTitle2(context),
                                    textAlign: TextAlign.end,
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    getHourFromTimestampFormat(event.dateTime),
                                    style: kTitle3(context),
                                    textAlign: TextAlign.end,
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    capitalize(event.location),
                                    style: kTitle4(context),
                                    textAlign: TextAlign.end,
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    capitalize(event.name),
                                    style: kTitle4(context),
                                    textAlign: TextAlign.end,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Container(
                          padding: const EdgeInsets.only(
                              top: 40.0, right: 20.0, left: 20.0, bottom: 30.0),
                          decoration: const BoxDecoration(
                            color: kMainBackground,
                            borderRadius: BorderRadius.vertical(
                              top: Radius.circular(30),
                            ),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              adminProvider.adminIsLoggedIn
                                  ? Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        GestureDetector(
                                          behavior: HitTestBehavior.translucent,
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        SetEventView(
                                                          event: event,
                                                        )));
                                          },
                                          child: const Icon(
                                            Icons.create,
                                            color: kWhiteText,
                                          ),
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        GestureDetector(
                                          behavior: HitTestBehavior.translucent,
                                          onTap: () async {
                                            await removeAlert(
                                                context: context,
                                                onTap: () async {
                                                  bool isRemove = await context
                                                      .read<EventViewModels>()
                                                      .remove();
                                                  if (isRemove) {
                                                    Navigator.pushAndRemoveUntil(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                const HomeView()),
                                                        (route) => false);
                                                  }
                                                });
                                          },
                                          child: const Align(
                                            alignment: Alignment.topRight,
                                            child: Icon(
                                              Icons.delete,
                                              color: kWhiteText,
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  : Container(),
                              Text(
                                'A propos',
                                style: kTitle3(context),
                              ),
                              Container(
                                margin:
                                    const EdgeInsets.only(top: 15.0, left: 5.0),
                                child: Text(
                                  '${event.description}',
                                  style: kDescriptionText(context),
                                ),
                              ),
                              const SizedBox(
                                height: 40,
                              ),
                              Text(
                                'Artistes',
                                style: kTitle3(context),
                              ),
                              Container(
                                margin: const EdgeInsets.only(top: 15.0),
                                height: 200,
                                child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  itemCount: event.artists?.length ?? 0,
                                  itemBuilder: (context, index) {
                                    if (event.artists != null) {
                                      return ArtistCard(
                                        artist: event.artists![index],
                                      );
                                    } else {
                                      return Container();
                                    }
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  } else {
                    return Container();
                  }
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}
