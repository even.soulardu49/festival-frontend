import 'dart:convert';

import 'package:festoche/models/artist.dart';
import 'package:festoche/utils/config.api.dart';
import 'package:festoche/utils/response.dart';
import 'package:http/http.dart' as http;

abstract class ArtistApiService {
  static Future<Response<List<Artist>>> getAllArtist() {
    return ConfigAPI.requestErrorHandler<List<Artist>>(
      request: http.get(
        ConfigAPI.getUrl('/artist'),
        headers: ConfigAPI.headers,
      ),
      cases: {
        200: (response) async {
          List<Artist> artists = [];
          final List<dynamic> jsonResponse = jsonDecode(response.body);
          print(jsonResponse);
          for (int i = 0; i < jsonResponse.length; i++) {
            artists.add(Artist.fromJson(jsonResponse[i]));
          }
          return Response.complete<List<Artist>>(data: artists);
        }
      },
    );
  }

  static Future<Response<Artist>> getArtistById(int id) {
    return ConfigAPI.requestErrorHandler<Artist>(
      request: http.get(
        ConfigAPI.getUrl('/artist/$id'),
        headers: ConfigAPI.headers,
      ),
      cases: {
        200: (response) async {
          final dynamic jsonResponse = jsonDecode(response.body);
          print(jsonResponse);
          Artist artist = Artist.fromJson(jsonResponse);
          return Response.complete<Artist>(data: artist);
        }
      },
    );
  }

  static Future<Response<void>> addArtist(Artist artist) {
    return ConfigAPI.requestErrorHandler<void>(
      request: http.post(
        ConfigAPI.getUrl('/artist'),
        headers: ConfigAPI.headers,
        body: jsonEncode({
          'name': artist.name,
          'image': artist.imgUrl,
          'description': artist.description,
          'events': artist.events
        }),
      ),
      cases: {
        201: (response) async {
          return Response.complete<void>();
        }
      },
    );
  }

  static Future<Response<void>> updateArtist(Artist artist) {
    return ConfigAPI.requestErrorHandler<void>(
      request: http.put(
        ConfigAPI.getUrl('/artist'),
        headers: ConfigAPI.headers,
        body: jsonEncode({
          'id': artist.id,
          'name': artist.name,
          'image': artist.imgUrl,
          'description': artist.description,
          'events': artist.events
        }),
      ),
      cases: {
        200: (response) async {
          return Response.complete<void>();
        }
      },
    );
  }

  static Future<Response<void>> removeArtist(int id) {
    return ConfigAPI.requestErrorHandler<void>(
      request: http.delete(
        ConfigAPI.getUrl('/artist'),
        headers: ConfigAPI.headers,
        body: jsonEncode({
          'id': id,
        }),
      ),
      cases: {
        200: (response) async {
          return Response.complete<void>();
        }
      },
    );
  }
}
