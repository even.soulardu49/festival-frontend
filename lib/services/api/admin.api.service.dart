import 'dart:convert';

import 'package:festoche/models/admin.dart';
import 'package:festoche/services/shared_preferences.service.dart';
import 'package:festoche/utils/config.api.dart';
import 'package:festoche/utils/response.dart';
import 'package:http/http.dart' as http;

abstract class AdminApiService {
  static Future<Response<void>> loginWithEmail(
      String email, String password) async {
    return await ConfigAPI.requestErrorHandler<void>(
      request: http.post(
        ConfigAPI.getUrl('/admin/login'),
        headers: ConfigAPI.headers,
        body: jsonEncode(
          <String, String>{"email": email, "password": password},
        ),
      ),
      cases: {
        201: (response) async {
          final Map<String, dynamic> jsonContent = jsonDecode(response.body);
          SharedPreferencesService.setUserAccessToken(
              jsonContent['access_token']);
          ConfigAPI.setToken(jsonContent['access_token']);
          return Response.complete<void>();
        }
      },
    );
  }

  static Future<Response<Admin>> getUserWithToken() {
    return ConfigAPI.requestErrorHandler<Admin>(
      request: http.get(
        ConfigAPI.getUrl('/admin/profile'),
        headers: ConfigAPI.headers,
      ),
      cases: {
        200: (response) async {
          final jsonResponse = jsonDecode(response.body);
          final Admin user = Admin.fromJson(jsonResponse);
          return Response.complete<Admin>(data: user);
        }
      },
    );
  }

  static void logout() {
    SharedPreferencesService.removeAccessToken();
    ConfigAPI.setToken('');
  }
}
