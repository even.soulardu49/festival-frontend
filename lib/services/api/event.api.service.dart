import 'dart:convert';

import 'package:festoche/models/event.dart';
import 'package:festoche/utils/config.api.dart';
import 'package:festoche/utils/response.dart';
import 'package:http/http.dart' as http;

abstract class EventApiService {
  static Future<Response<List<Event>>> getAllEvents() async {
    return ConfigAPI.requestErrorHandler<List<Event>>(
      request: http.get(
        ConfigAPI.getUrl('/event'),
        headers: ConfigAPI.headers,
      ),
      cases: {
        200: (response) async {
          List<Event> events = [];
          final List<dynamic> jsonResponse = jsonDecode(response.body);
          for (int i = 0; i < jsonResponse.length; i++) {
            events.add(Event.fromJson(jsonResponse[i]));
          }
          return Response.complete<List<Event>>(data: events);
        }
      },
    );
  }

  static Future<Response<Event>> getEventById(int id) {
    return ConfigAPI.requestErrorHandler<Event>(
      request: http.get(
        ConfigAPI.getUrl('/event/$id'),
        headers: ConfigAPI.headers,
      ),
      cases: {
        200: (response) async {
          final dynamic jsonResponse = jsonDecode(response.body);
          print(jsonResponse);
          Event event = Event.fromJson(jsonResponse);
          return Response.complete<Event>(data: event);
        }
      },
    );
  }

  static Future<Response<void>> addEvent(Event event) {
    return ConfigAPI.requestErrorHandler<void>(
      request: http.post(
        ConfigAPI.getUrl('/event'),
        headers: ConfigAPI.headers,
        body: jsonEncode(event.toJson()),
      ),
      cases: {
        201: (response) async {
          return Response.complete<void>();
        }
      },
    );
  }

  static Future<Response<void>> updateEvent(Event event) {
    return ConfigAPI.requestErrorHandler<void>(
      request: http.put(
        ConfigAPI.getUrl('/event'),
        headers: ConfigAPI.headers,
        body: jsonEncode(event.toJson()),
      ),
      cases: {
        200: (response) async {
          return Response.complete<void>();
        }
      },
    );
  }

  static Future<Response<void>> removeEvent(int id) {
    return ConfigAPI.requestErrorHandler<void>(
      request: http.delete(
        ConfigAPI.getUrl('/event'),
        headers: ConfigAPI.headers,
        body: jsonEncode({
          'id': id,
        }),
      ),
      cases: {
        200: (response) async {
          return Response.complete<void>();
        }
      },
    );
  }
}
