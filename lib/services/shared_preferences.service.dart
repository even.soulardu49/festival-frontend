import 'package:shared_preferences/shared_preferences.dart';

abstract class SharedPreferencesService {
  static Future<void> setUserAccessToken(String accessToken) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('user_access_token', accessToken);
  }

  static Future<void> removeAccessToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('user_access_token');
  }
}
