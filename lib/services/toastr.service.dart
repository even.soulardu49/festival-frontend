import 'dart:ui';

import 'package:festoche/utils/colors.dart';
import 'package:festoche/utils/enums.dart';
import 'package:flash/flash.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class ToastrService {
  // Store last request
  static _ToastrData? _toastrData;

  // Store last controller to be able to dismiss it
  static FlashController? _toastrController;

  static Future<void> show(BuildContext context, String message,
      {required ToastrType type,
      int? durationInSeconds,
      VoidCallback? onTap}) async {
    // Check clones
    final toastrData = _ToastrData(
      message: message,
    );
    if (toastrData != _toastrData) {
      _toastrData = toastrData;
      //Try to get higher level context, so the Flash message's position is relative to the phone screen (and not a child widget)
      try {
        // May throw if context in not on the tree anymore
        final scaffoldContext = Scaffold.maybeOf(context)?.context;
        if (scaffoldContext != null) context = scaffoldContext;

        //Dismiss previous message
        _toastrController?.dismiss();

        //Display new message
        await showFlash(
          context: context,
          duration: Duration(seconds: durationInSeconds ?? 3),
          builder: (context, controller) {
            final backgroundColor = type == ToastrType.error
                ? Colors.red
                : type == ToastrType.success
                    ? Colors.green
                    : Colors.lightBlue;
            _toastrController = controller;

            return ToastrWidget(
              controller: _toastrController!,
              backgroundColor: backgroundColor,
              type: type,
              message: message,
              onTap: onTap,
            );
          },
        );

        _toastrData = null;
        _toastrController = null;
      } catch (e) {
        // Just ignore and don't display message.
        print(e);
      }
    }
  }
}

class ToastrWidget extends StatelessWidget {
  const ToastrWidget({
    Key? key,
    required this.backgroundColor,
    required this.controller,
    this.onTap,
    required this.type,
    required this.message,
  }) : super(key: key);

  final MaterialColor backgroundColor;
  final FlashController controller;
  final void Function()? onTap;
  final ToastrType type;
  final String message;

  @override
  Widget build(BuildContext context) {
    return Flash(
      behavior: FlashBehavior.floating,
      controller: controller,
      backgroundColor: backgroundColor,
      margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
      position: FlashPosition.top,
      brightness: Brightness.dark,
      horizontalDismissDirection: HorizontalDismissDirection.horizontal,
      borderRadius: BorderRadius.circular(8.0),
      boxShadows: kElevationToShadow[8],
      onTap: () {
        controller.dismiss();
        onTap?.call();
      },
      child: ConstrainedBox(
        constraints: const BoxConstraints(maxWidth: 350),
        child: FlashBar(
          content: Row(
            children: [
              // Icon
              Icon(
                type == ToastrType.error
                    ? Icons.warning
                    : type == ToastrType.success
                        ? Icons.check
                        : Icons.info,
                color: kWhiteText,
                size: 30,
              ),

              // Message
              const SizedBox(width: 10),
              Expanded(
                child: Text(
                  message,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText2!
                      .apply(color: kWhiteText),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _ToastrData {
  const _ToastrData({required this.message});

  final String message;

  @override
  bool operator ==(Object o) => o is _ToastrData && message == o.message;

  @override
  int get hashCode => message.hashCode;
}
